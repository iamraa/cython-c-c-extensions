#include <stdlib.h>
#include <stdio.h>

static char* inside_c(char* s)
{
	int required_size = 100;
	// Reserve memory for return variable
	char *r = (char *)malloc(sizeof (char) * required_size);

	// Change string
	sprintf(r, "we get: %s", s);

	return r;
}